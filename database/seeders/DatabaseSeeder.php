<?php

namespace Database\Seeders;

use App\Models\Bin;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        User::factory()->create([
            'name' => 'Faustino Vasquez',
            'email' => 'fvasquez@local.com'
        ]);

        Product::factory()->times(6)->create();

        Bin::factory()->times(2)->create();
    }
}
