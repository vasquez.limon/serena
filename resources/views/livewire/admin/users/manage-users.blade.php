<div>
    <x-jet-button wire:click="createShowModal" class="bg-blue-500 hover:bg-blue-700">
        <i class="fas fa-plus"></i>&nbsp; {{ __('Create User') }}
    </x-jet-button>



    <x-jet-dialog-modal wire:model="modalFormVisible" maxWidth="2xl">
        <x-slot name="title">
            @if ($showEditModal)
            {{ __('Update User') }}
            @else
            {{ __('Create New User') }}
            @endif
        </x-slot>

        <x-slot name="content">
            <div>
                <x-jet-label for="name" value="{{ __('Name') }}" />
                <x-jet-input wire:model.defer='state.name' id="name" class="block mb-3 w-full" type="text" name="name"
                    placeholder="Enter Name..." required autofocus />
                <x-jet-input-error for="name" class="mt-2" />
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input wire:model.defer='state.email' id="email" class="block mt-1 w-full" type="email"
                    name="email" placeholder="Enter Email..." required autofocus />
                <x-jet-input-error for="email" class="mt-2" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mb-3 w-full" type="password" name="password"
                    wire:model.defer='state.password' placeholder="Enter password..." required autofocus />
                <x-jet-input-error for="password" class="mt-2" />
            </div>
            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Password Confirmation') }}" />
                <x-jet-input id="password_confirmation" class="block mb-3 w-full" type="password"
                    name="password_confirmation" wire:model.defer='state.password_confirmation'
                    placeholder="Enter password..." required autofocus />
                <x-jet-input-error for="password" class="mt-2" />
            </div>
        </x-slot>

        <x-slot name="footer">
            <x-jet-secondary-button wire:click="$toggle('modalFormVisible')" wire:loading.attr="disabled">
                {{ __('Nevermind') }}
            </x-jet-secondary-button>

            @if ($showEditModal)
            <x-jet-button class="ml-2" wire:click="update" wire:loading.attr="disabled">
                {{ __('Update') }}
            </x-jet-button>
            @else
            <x-jet-button class="ml-2" wire:click="store" wire:loading.attr="disabled">
                {{ __('Create') }}
            </x-jet-button>
            @endif
        </x-slot>
    </x-jet-dialog-modal>
</div>
