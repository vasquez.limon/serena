<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('New Resident') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            <x-jet-form-section submit="save">
                <x-slot name="title">
                    {{ __('Resident Information') }}
                </x-slot>

                <x-slot name="description">
                    {{ __('Information about Residents.') }}
                </x-slot>

                <x-slot name="form">

                    @if ($houses->count())
                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="name" value="{{ __('Resident Name') }}" />
                        <x-jet-input id="name" type="text" class="mt-1 block w-full" wire:model="resident.name"
                            autocomplete="Resident name" />
                        <x-jet-input-error for="resident.name" class="mt-2" />
                    </div>
                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="slug" value="{{ __('Slug') }}" />
                        <x-jet-input id="slug" type="text" class="mt-1 block w-full" wire:model="resident.slug"
                            autocomplete="Slug" />
                        <x-jet-input-error for="resident.slug" class="mt-2" />
                    </div>

                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="cam" value="{{ __('Resident Cam') }}" />
                        <x-jet-input id="cam" type="text" class="mt-1 block w-full" wire:model="resident.cam"
                            autocomplete="Cam" />
                        <x-jet-input-error for="resident.cam" class="mt-2" />
                    </div>

                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="house_id" value="{{ __('Resident House') }}" />
                        <select name="house_id" id="house_id" wire:model="resident.house_id"
                            class="border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm mt-1 block w-full">
                            <option value="">Select House</option>
                            @foreach ($houses as $house )
                            <option value="{{$house->id}}">{{$house->name}}</option>
                            @endforeach
                        </select>
                        <x-jet-input-error for="resident.house_id" class="mt-2" />
                    </div>

                    <x-slot name="actions">
                        <x-jet-button wire:loading.attr="disabled">
                            {{ __('Save') }}
                        </x-jet-button>
                    </x-slot>

                    @else
                    <div class="col-span-6 sm:col-span-6">
                        <div class="flex justify-between">
                            <div>
                                {{__('There are not Houses created, create one first')}}
                            </div>

                            <div>
                                <a class="flex items-center" href="{{route("admin.houses.create")}}">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                                    </svg>
                                    <span class="ml-1">Create House</span>
                                </a>
                            </div>

                        </div>

                    </div>
                    @endif





                </x-slot>
            </x-jet-form-section>
        </div>
    </div>
</div>
