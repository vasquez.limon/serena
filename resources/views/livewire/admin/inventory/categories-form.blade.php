<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('New Category') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            <x-jet-form-section submit="save">
                <x-slot name="title">
                    {{ __('Category Information') }}
                </x-slot>

                <x-slot name="description">
                    {{ __('Information about Categories.') }}
                </x-slot>

                <x-slot name="form">

                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="name" value="{{ __('Category Name') }}" />
                        <x-jet-input id="name" type="text" class="mt-1 block w-full" wire:model="category.name"
                            autocomplete="Category name" />
                        <x-jet-input-error for="category.name" class="mt-2" />
                    </div>
                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="slug" value="{{ __('Slug') }}" />
                        <x-jet-input id="slug" type="text" class="mt-1 block w-full" wire:model="category.slug"
                            autocomplete="Slug" />
                        <x-jet-input-error for="category.slug" class="mt-2" />
                    </div>
                    <x-slot name="actions">
                        <x-jet-button wire:loading.attr="disabled">
                            {{ __('Save') }}
                        </x-jet-button>
                    </x-slot>

                </x-slot>
            </x-jet-form-section>
        </div>
    </div>
</div>
