<div class="py-12">

    <x-slot name="header">
        <div class="flex items-center justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('List Products') }}
            </h2>
            <div class="mr-2">
                <a href="{{route("admin.products.create")}}">Create Product</a>
            </div>

        </div>
    </x-slot>

    <div class=" max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            @if($products->count())
            <x-table>
                <table class="w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50">
                        <tr>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Name
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Registered Date
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Concentration
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Category
                            </th>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Created By
                            </th>

                            <th scope="col" class="relative px-6 py-3">
                                <span class="sr-only">Options</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                        @foreach ($products as $product)
                        <tr class="hover:bg-gray-100 text-gray-900">
                            <td class="px-6 py-4 whitespace-nowrap">
                                {{ $product->name }}
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap">
                                {{ $product->created_at->diffForHumans() }}
                            </td>

                            <td class="px-6 py-4 whitespace-nowrap">
                                {{ $product->concentration }}
                            </td>

                            <td class="px-6 py-4 whitespace-nowrap">
                                {{ $product->category->name }}
                            </td>

                            <td class="px-6 py-4 whitespace-nowrap">
                                {{ $product->user->name }}
                            </td>

                            <td class="px-6 py-4 whitespace-nowrap text-center text-sm font-medium">

                                <a href="{{route('admin.products.edit', $product)}}">
                                    editar
                                </a>
                                {{-- <x-jet-button
                                                wire:click="$emitTo('admin.products.manage-users', 'updateUser',{{ $product->id }}
                                )" class="bg-green-500 hover:bg-green-700 ">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24"
                                    stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                        d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                </svg>
                                </x-jet-button> --}}

                                <x-jet-button
                                    {{-- wire:click="$emitTo('admin.products.manage-users', 'confirmDelete',{{ $product->id }}
                                    )" --}} class="bg-red-500 hover:bg-red-700">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                    </svg>
                                </x-jet-button>
                            </td>
                        </tr>
                        @endforeach


                        <!-- More people... -->
                    </tbody>
                </table>
                <div class="bg-white px-4 py-3  border-t border-gray-200 sm:px-6">
                    {{$products->links()}}
                </div>
            </x-table>
            @else
            <div class="bg-white px-4 py-3 sm:px-6 text-center">
                {{__('There are not products created')}}
            </div>
            @endif
        </div>
    </div>
</div>
