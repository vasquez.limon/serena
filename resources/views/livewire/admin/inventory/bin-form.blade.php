<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('New Bin') }}
        </h2>
    </x-slot>

    <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
        @if($allResidents->count())

        <x-jet-form-section submit="save">
            <x-slot name="title">
                {{ __('Bin Information') }}
            </x-slot>

            <x-slot name="description">
                {{ __('Information about Bins.') }}
            </x-slot>

            <x-slot name="form">

                <div class="col-span-6 sm:col-span-4">
                    <x-jet-label for="name" value="{{ __('Bin Name') }}" />
                    <x-jet-input id="name" type="text" class="mt-1 block w-full" wire:model="bin.name"
                        autocomplete="Bin name" />
                    <x-jet-input-error for="bin.name" class="mt-2" />
                </div>
                <div class="col-span-6 sm:col-span-4">
                    <x-jet-label for="slug" value="{{ __('Slug') }}" />
                    <x-jet-input id="slug" type="text" class="mt-1 block w-full" wire:model="bin.slug"
                        autocomplete="Slug" />
                    <x-jet-input-error for="bin.slug" class="mt-2" />
                </div>
                <div class="col-span-6 sm:col-span-4">
                    <x-jet-label for="code" value="{{ __('Bin Code') }}" />
                    <x-jet-input id="code" type="text" class="mt-1 block w-full" wire:model="bin.code"
                        autocomplete="Code" />
                    <x-jet-input-error for="bin.code" class="mt-2" />
                </div>
                <div class="col-span-6 sm:col-span-4">
                    <x-jet-label for="resident_id" value="{{ __('Resident') }}" />
                    <select name="resident_id" id="resident_id" wire:model="bin.resident_id"
                        class="border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm mt-1 block w-full">
                        <option value="">Select Resident</option>
                        @foreach ($allResidents as $resident )
                        <option value="{{$resident->id}}">{{$resident->name}}</option>
                        @endforeach
                    </select>
                    <x-jet-input-error for="bin.resident_id" class="mt-2" />
                </div>
                <div class="col-span-6">
                    <hr>
                </div>
                <div class="col-span-6">
                    <table class="w-full">
                        <tr>
                            @foreach ($binProducts as $index => $binProduct)
                            <td width="60%">
                                <select name="binProducts[{{$index}}][product_id]"
                                    wire:model="binProducts.{{$index}}.product_id"
                                    class=" w-full border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm">
                                    <option value="">-- choose product --</option>
                                    @foreach ($allProducts as $product)
                                    <option value="{{ $product->id }}">
                                        {{ $product->name }} (${{ number_format($product->price, 2) }})
                                    </option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <x-jet-input type="number" min="1" name="binProducts[{{$index}}][quantity]"
                                    wire:model="binProducts.{{$index}}.quantity" class="block w-full" />

                            </td>
                            <td class="text-right">

                                <a href=" #" wire:click.prevent="removeProduct({{$index}})"
                                    class="text-blue-500 flex justify-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                    </svg>
                                    <span class="ml-1">Delete</span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </table>

                    <x-jet-button wire:click.prevent="addProduct" class="mt-5">
                        {{ __('+ Add Another Product') }}
                    </x-jet-button>
                </div>
            </x-slot>

            <x-slot name="actions">
                <x-jet-button wire:loading.attr="disabled">
                    {{ __('Save') }}
                </x-jet-button>
            </x-slot>

        </x-jet-form-section>

        @else
        <div class="col-span-6 sm:col-span-6">
            <div class="flex justify-between">
                <div>
                    {{__('There are not Residents created, create one first')}}
                </div>

                <div>
                    <a class="flex items-center" href="{{route("admin.residents.create")}}">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24"
                            stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="M18 9v3m0 0v3m0-3h3m-3 0h-3m-2-5a4 4 0 11-8 0 4 4 0 018 0zM3 20a6 6 0 0112 0v1H3v-1z" />
                        </svg>
                        <span class="ml-1">Create Resident</span>
                    </a>
                </div>

            </div>

        </div>
        @endif
    </div>
</div>
