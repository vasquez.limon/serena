<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('New Product') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            <x-jet-form-section submit="save">
                <x-slot name="title">
                    {{ __('Product Information') }}
                </x-slot>

                <x-slot name="description">
                    {{ __('Information about Products.') }}
                </x-slot>

                <x-slot name="form">
                    @if($categories->count())

                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="name" value="{{ __('Product Name') }}" />
                        <x-jet-input id="name" type="text" class="mt-1 block w-full" wire:model="product.name"
                            autocomplete="Product name" />
                        <x-jet-input-error for="product.name" class="mt-2" />
                    </div>
                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="slug" value="{{ __('Slug') }}" />
                        <x-jet-input id="slug" type="text" class="mt-1 block w-full" wire:model="product.slug"
                            autocomplete="Slug" />
                        <x-jet-input-error for="product.slug" class="mt-2" />
                    </div>

                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="concentration" value="{{ __('Concentration') }}" />
                        <x-jet-input id="concentration" type="text" class="mt-1 block w-full"
                            wire:model="product.concentration" autocomplete="Concentration" />
                        <x-jet-input-error for="product.concentration" class="mt-2" />
                    </div>

                    <div class="col-span-6 sm:col-span-4">
                        <x-jet-label for="category_id" value="{{ __('Category') }}" />
                        <select name="category_id" id="category_id" wire:model="product.category_id"
                            class="border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm mt-1 block w-full">
                            <option value="">Select Category</option>
                            @foreach ($categories as $category )
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        <x-jet-input-error for="product.category_id" class="mt-2" />
                    </div>

                    <x-slot name="actions">
                        <x-jet-button wire:loading.attr="disabled">
                            {{ __('Save') }}
                        </x-jet-button>
                    </x-slot>
                    @else
                    <div class="col-span-6 sm:col-span-6">
                        <div class="flex justify-between">
                            <div>
                                {{__('There are not CATEGORIES created, create one first')}}
                            </div>

                            <div>
                                <a class="flex items-center" href="{{route("admin.categories.create")}}">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M5 13l4 4L19 7" />
                                    </svg>
                                    <span class="ml-1">Create Category</span>
                                </a>
                            </div>

                        </div>

                    </div>

                    @endif

                </x-slot>
            </x-jet-form-section>
        </div>
    </div>
</div>
