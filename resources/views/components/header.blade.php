@props(['title' => null])
<div class="bg-white shadow">
    <div class="flex items-baseline justify-between max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
        <h2 class=" font-semibold text-xl text-gray-800 leading-tight">
            {{ $title }}
        </h2>
        <div class="mr-2">

            {{$slot}}

        </div>
    </div>
</div>
