<?php

namespace App\Http\Livewire\Admin\Houses;

use App\Models\House;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $search = '';

    public function render()
    {
        return view('livewire.admin.houses.index', [
            'houses' => House::where('name', 'like', "%{$this->search}%")
                ->latest()
                ->paginate()
        ]);
    }
}
