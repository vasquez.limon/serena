<?php

namespace App\Http\Livewire\Admin\Houses;

use App\Models\House;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Illuminate\Support\Str;

class Form extends Component
{
    public House $house;


    protected function rules()
    {

        return [
            'house.name' => ['required', 'min:4'],
            'house.slug' => [
                'required',
                'alpha_dash',
                Rule::unique('houses', 'slug')->ignore($this->house)
            ],
            'house.address' => ['required']
        ];
    }

    public function mount(House $house)
    {
        $this->house = $house;
    }


    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }


    public function updatedhouseName($name)
    {
        $this->house->slug = Str::slug($name);
    }


    public function save()
    {
        $this->validate();

        Auth::user()->houses()->save($this->house);

        session()->flash('status', __('House saved.'));
        $this->redirectRoute('admin.houses.index');
    }


    public function render()
    {
        return view('livewire.admin.houses.form');
    }
}
