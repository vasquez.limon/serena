<?php

namespace App\Http\Livewire\Admin\Inventory;

use Livewire\Component;

class BinProducts extends Component
{
    public function render()
    {
        return view('livewire.admin.inventory.bin-products');
    }
}
