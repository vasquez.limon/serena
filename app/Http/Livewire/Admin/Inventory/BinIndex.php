<?php

namespace App\Http\Livewire\Admin\Inventory;

use App\Models\Bin;
use Livewire\Component;
use Livewire\WithPagination;

class BinIndex extends Component
{
    use WithPagination;

    public $search = '';


    public function render()
    {


        return view('livewire.admin.inventory.bin-index', [
            'bins' => Bin::where('name', 'like', "%{$this->search}%")
                ->latest()
                ->paginate()
        ]);
    }
}
