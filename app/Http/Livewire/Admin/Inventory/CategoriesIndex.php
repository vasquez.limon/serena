<?php

namespace App\Http\Livewire\Admin\Inventory;

use App\Models\Category;
use Livewire\Component;
use Livewire\WithPagination;

class CategoriesIndex extends Component
{
    use WithPagination;

    public $search = '';

    public function render()
    {
        return view('livewire.admin.inventory.categories-index', [
            'categories' => Category::where('name', 'like', "%{$this->search}%")
                ->latest()
                ->paginate()
        ]);
    }
}
