<?php

namespace App\Http\Livewire\Admin\Inventory;

use App\Models\Bin;
use App\Models\Product;
use App\Models\Resident;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Illuminate\Support\Str;


class BinForm extends Component
{
    public Bin $bin;


    public $binProducts = [];
    public $allProducts = [];
    public $allResidents = [];


    protected function rules()
    {

        return [
            'bin.name' => ['required', 'min:4'],
            'bin.slug' => [
                'required',
                'alpha_dash',
                Rule::unique('bins', 'slug')->ignore($this->bin)
            ],
            'bin.code' => [
                'required',
                'min:4',
                Rule::unique('bins', 'code')->ignore($this->bin)
            ],
            'bin.resident_id' => ['required'],

            'binProducts' => ['required', 'array', 'min:1'],
        ];
    }

    public function mount(Bin $bin)
    {
        $this->bin = $bin;

        $this->allResidents = Resident::all();
        $this->allProducts = Product::all();

        if ($this->bin->id) {
            $products = $this->bin->products;
            foreach ($products as $product) {

                $arr[$product->pivot->product_id] = $product->pivot->quantity;
            }

            dd($arr);
        } else {
            $this->binProducts = [
                ['product_id' => '', 'quantity' => 1]
            ];
        }
    }

    /**
     * the Add Products to bin function
     *
     * @return void
     */
    public function addProduct()
    {
        $this->binProducts[] = ['product_id' => '', 'quantity' => 1];
    }

    /**
     * the remove Products to bin function
     *
     * @return void
     */
    public function removeProduct($index)
    {
        unset($this->binProducts[$index]);
        $this->binProducts = array_values($this->binProducts);
    }



    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }


    public function updatedBinName($name)
    {
        $this->bin->slug = Str::slug($name);
    }


    public function save()
    {
        $this->validate();

        $bin = Auth::user()->bins()->save($this->bin);

        foreach ($this->binProducts as $product) {
            $bin->products()->attach(
                $product['product_id'],
                ['quantity' => $product['quantity']]
            );
        }

        $this->resetVars();

        session()->flash('status', __('Bin saved.'));
        $this->redirectRoute('admin.bins.index');
    }


    public function resetVars()
    {
        // $this->modalFormVisible = false;
        // $this->name = null;
        // $this->code = null;
        // $this->resident = null;
        $this->binProducts = [
            ['product_id' => '', 'quantity' => 1]
        ];
    }


    public function render()
    {
        return view('livewire.admin.inventory.bin-form');
    }
}
