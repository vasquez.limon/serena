<?php

namespace App\Http\Livewire\Admin\Inventory;

use App\Models\Bin;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Illuminate\Support\Str;

class ProductsForm extends Component
{

    public Product $product;
    public $categories;
    public $bin;


    protected function rules()
    {

        return [
            'product.name' => ['required', 'min:4'],
            'product.slug' => [
                'required',
                'alpha_dash',
                Rule::unique('products', 'slug')->ignore($this->product)
            ],
            'product.concentration' => ['required', 'min:4'],
            'product.qty' => ['required'],
            'product.category_id' => ['required'],
        ];
    }

    public function mount(Product $product)
    {
        $this->product = $product;
        $this->categories = Category::all();
    }


    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }


    public function updatedProductName($name)
    {
        $this->product->slug = Str::slug($name);
    }


    public function save()
    {
        $this->validate();

        Auth::user()->products()->save($this->product);

        session()->flash('status', __('Product saved.'));
        $this->redirectRoute('admin.products.index');
    }

    public function render()
    {
        return view('livewire.admin.inventory.products-form');
    }
}
