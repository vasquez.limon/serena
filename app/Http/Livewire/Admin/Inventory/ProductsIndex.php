<?php

namespace App\Http\Livewire\Admin\Inventory;

use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;

class ProductsIndex extends Component
{
    use WithPagination;

    public $search = '';

    public function render()
    {
        return view('livewire.admin.inventory.products-index', [
            'products' => Product::where('name', 'like', "%{$this->search}%")
                ->latest()
                ->paginate()
        ]);
    }
}
