<?php

namespace App\Http\Livewire\Admin\Inventory;

use App\Models\Category;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Illuminate\Support\Str;

class CategoriesForm extends Component
{
    public Category $category;


    protected function rules()
    {

        return [
            'category.name' => ['required', 'min:4'],
            'category.slug' => [
                'required',
                'alpha_dash',
                Rule::unique('categories', 'slug')->ignore($this->category)
            ],
        ];
    }

    public function mount(Category $category)
    {
        $this->category = $category;
    }


    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }


    public function updatedCategoryName($name)
    {
        $this->category->slug = Str::slug($name);
    }


    public function save()
    {
        $this->validate();

        $this->category->save();

        session()->flash('status', __('Category saved.'));
        $this->redirectRoute('admin.categories.index');
    }




    public function render()
    {
        return view('livewire.admin.inventory.categories-form');
    }
}
