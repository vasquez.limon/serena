<?php

namespace App\Http\Livewire\Admin\Users;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Livewire\Component;

class ManageUsers extends Component
{
    public $state = [];
    public $user;
    public $userBeingRemoved = null;
    public $modalFormVisible = false;
    public $showEditModal = false;


    protected $listeners = [
        'updateUser' => 'updateShowModal',
        'confirmDelete' => 'confirmUserRemoval',
        'deleteConfirmed' => 'deleteUser'
    ];


    public function createShowModal()
    {
        $this->reset();
        $this->showEditModal = false;
        $this->modalFormVisible = true;
    }

    public function store()
    {
        $validatedData = Validator::make($this->state, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ])->validate();

        $validatedData['password'] = bcrypt($validatedData['password']);

        User::create($validatedData);
        $this->reset();
        $this->emitTo('admin.users.list-users', 'render');
        $this->dispatchBrowserEvent('success', ['message' => 'User created successfully']);
    }


    public function updateShowModal(User $user)
    {
        $this->reset();
        $this->showEditModal = true;
        $this->user = $user;
        $this->state = $user->toArray();
        $this->modalFormVisible = true;
    }

    public function update()
    {
        $validatedData = Validator::make($this->state, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $this->user->id,
            'password' => 'sometimes|confirmed',
        ])->validate();

        if (!empty($validatedData['password'])) {
            $validatedData['password'] = bcrypt($validatedData['password']);
        }

        $this->user->update($validatedData);
        $this->reset();
        $this->emitTo('admin.users.list-users', 'render');
        $this->dispatchBrowserEvent('success', ['message' => 'User updated successfully']);
    }


    public function confirmUserRemoval($userId)
    {
        $this->userBeingRemoved = $userId;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }


    public function deleteUser()
    {
        $user = User::findOrFail($this->userBeingRemoved);
        $user->delete();
        $this->emitTo('admin.users.list-users', 'render');
        $this->dispatchBrowserEvent('success', ['message' => 'User deleted successfully']);
    }


    public function render()
    {
        return view('livewire.admin.users.manage-users');
    }
}
