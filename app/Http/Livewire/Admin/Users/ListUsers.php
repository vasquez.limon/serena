<?php

namespace App\Http\Livewire\Admin\Users;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class ListUsers extends Component
{

    use WithPagination;


    public $searchTerm = null;

    protected $listeners = [
        'render'
    ];

    public function render()
    {
        $users = User::query()
            ->where('name', 'like', '%' . $this->searchTerm . '%')
            ->orWhere('email', 'like', '%' . $this->searchTerm . '%')
            ->latest()->paginate(10);
        return view('livewire.admin.users.list-users', compact('users'));
    }
}
