<?php

namespace App\Http\Livewire\Admin\Residents;

use App\Models\Resident;
use Livewire\Component;
use Livewire\WithPagination;

class ResidentIndex extends Component
{
    use WithPagination;

    public $search = '';

    public function render()
    {
        return view('livewire.admin.residents.resident-index', [
            'residents' => Resident::where('name', 'like', "%{$this->search}%")
                ->latest()
                ->paginate()
        ]);
    }
}
