<?php

namespace App\Http\Livewire\Admin\Residents;

use App\Models\House;
use App\Models\Resident;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Illuminate\Support\Str;

class ResidentForm extends Component
{
    public Resident $resident;
    public $houses;


    protected function rules()
    {
        return [
            'resident.name' => ['required', 'min:4'],
            'resident.slug' => [
                'required',
                'alpha_dash',
                Rule::unique('residents', 'slug')->ignore($this->resident)
            ],
            'resident.cam' => ['required'],
            'resident.house_id' => ['required']
        ];
    }

    public function mount(Resident $resident)
    {
        $this->resident = $resident;
        $this->houses = House::all();
    }


    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }


    public function updatedResidentName($name)
    {
        $this->resident->slug = Str::slug($name);
    }


    public function save()
    {
        $this->validate();

        Auth::user()->residents()->save($this->resident);

        session()->flash('status', __('Resident saved.'));
        $this->redirectRoute('admin.residents.index');
    }


    public function render()
    {
        return view('livewire.admin.residents.resident-form');
    }
}
