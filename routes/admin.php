<?php

use App\Http\Livewire\Admin\Houses\Form;
use App\Http\Livewire\Admin\Houses\Index;
use App\Http\Livewire\Admin\Inventory\BinForm;
use App\Http\Livewire\Admin\Inventory\BinIndex;
use App\Http\Livewire\Admin\Inventory\CategoriesForm;
use App\Http\Livewire\Admin\Inventory\CategoriesIndex;
use App\Http\Livewire\Admin\Inventory\ProductsForm;
use App\Http\Livewire\Admin\Inventory\ProductsIndex;
use App\Http\Livewire\Admin\Residents\ResidentForm;
use App\Http\Livewire\Admin\Residents\ResidentIndex;
use App\Http\Livewire\Admin\Users\ListUsers;
use Illuminate\Support\Facades\Route;



// Route::get('dashboard', DashboardController::class)->name('dashboard');
Route::get('users', ListUsers::class)->name('users');
// Route::get('settings', UpdateSetting::class)->name('settings');



Route::get('houses', Index::class)
    ->name('houses.index');

Route::get('houses/create', Form::class)
    ->name('houses.create');

Route::get('houses/{house}/edit', Form::class)
    ->name('houses.edit');

//Residents
Route::get('residents', ResidentIndex::class)
    ->name('residents.index');

Route::get('residents/create', ResidentForm::class)
    ->name('residents.create');

Route::get('residents/{resident}/edit', ResidentForm::class)
    ->name('residents.edit');


//Categories
Route::get('categories', CategoriesIndex::class)
    ->name('categories.index');

Route::get('categories/create', CategoriesForm::class)
    ->name('categories.create');

Route::get('categories/{category}/edit', CategoriesForm::class)
    ->name('categories.edit');


//Products
Route::get('products', ProductsIndex::class)
    ->name('products.index');

Route::get('products/create', ProductsForm::class)
    ->name('products.create');

Route::get('products/{product}/edit', ProductsForm::class)
    ->name('products.edit');

//Products
Route::get('bins', BinIndex::class)
    ->name('bins.index');

Route::get('bins/create', BinForm::class)
    ->name('bins.create');

Route::get('bins/{bin}/edit', BinForm::class)
    ->name('bins.edit');
